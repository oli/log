---
title: a log for anxiety
date: 2021-02-04 09:31:22
---

Well, yes, that's not a very nerd or exciting topic at all.

But it is one of those things that do tend to take over nerdiness and excitement.

It's that quietness you feel when a storm approaches in the horizon. And you know everything is so still and silent, and yet there it is. Coming. Silently. That pang of despair.

Only, you see, the storm is only in your head. It's not really there at all. You see it, you feel it, you know it. But it's not there. It won't leave you alone.

But it doesn't exist.

It never arrives.

And you can't get rid of it.
