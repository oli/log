---
title: a log of monospaced fonts
date: 2020-02-18 16:55:59
---

Some favorites in the order I discovered them:

[Inconsolata](https://github.com/googlefonts/Inconsolata) << [Iosevka](https://github.com/be5invis/Iosevka) << [Cozette](https://github.com/slavfox/Cozette) << [ypn envypn](https://github.com/hicolour/envypn-font)

Settled (for now) with _ypn envypn_ because it's a bit more spacious than _Cozette_, and it scales better. _Cozette_ is pretty good looking, though.

There's something about monospaced fonts that makes it addictive. All other fonts seem like a bit too much after a while.

And then there are bitmap fonts; after you get used to them, everything else seems blurry.

![screenshot of post in editor](/img/screenshot_20200218.png)
