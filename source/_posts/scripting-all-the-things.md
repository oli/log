---
title: scripting all the things
date: 2021-02-20 21:27:53
---

```
#!/bin/bash

hexo new post $1 | micro $(awk '{print $3}')
```

Very likely not the most elegant solution. The input must be something that can be turned into an URL (like with dashes between words) and then I can edit the title afterwards in the frontmatter. 

Also `awk` always feels a bit like cheating.

But I do need to stop thinking posting something here or on my main blog demands any more complication than a two-letter command.
