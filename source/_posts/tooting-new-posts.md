---
title: tooting new posts
date: 2021-03-19 12:40:21
---

Yeee new script that works! With variables!

This uses `rsstail` and `toot` (a cli mastodon client). It gets the last entry from my blog's rss feed and posts the link to mastodon.

SHOULD WORK. You can just change the feed address and mastodon accounts (after you log in to them on toot) to try it yourself.

```
#!/usr/bin/env bash

entry_title=$(rsstail -1 -n 1 -l -u log.oliviamaia.net/atom.xml | grep Title | sed 's/Title: //')
entry_link=$(rsstail -1 -n 1 -l -u log.oliviamaia.net/atom.xml | grep Link | awk '{print $2}')

toot activate olivia@eldritch.cafe
toot post "new nerdlog entry: $entry_title - $entry_link :)"

toot activate olivia@masto.donte.com.br
toot post "nova entrada no meu nerdlog (en): $entry_title - $entry_link :)"
```

It's still a MANUAL solution to stuff like IFTTT and Zapier and the like, but it looks kinda cool, right? Since I publish new posts and deploy my websites from the command line anyway, doesn't add that much complication to the process.
