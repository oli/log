---
title: about
layout: about
---

My name is Olivia Maia. I'm a Brazilian writer & artist, cat person & inux nerd. This blog is supposed to be a simple space to write about linux things and other nerdiness, register ideas, document findings and occasionally babble about anxiety and cats.

I'm no coder and I often crash things because I forgot an amperstand.

Sometimes I'm the worse artisan in the history of artisanship.

You can find my CC-licensed art on the [English side of my main website](https://oliviamaia.net/en).

Here's the usual glimpse into my current machine:

![neofetch screenshot](/img/screenshot_20210201-103250_529x348.png)

This blog is [open source](https://codeberg.org/olivia/log). It is made with [Hexo](https://hexo.io) and hosted on [Netlify](https://netlify.com). You're welcome to PR corrections and/or suggestions.
